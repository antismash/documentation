Cluster Types
-------------

antiSMASH uses some abbreviations internally to refer to the different
types of secondary metabolite clusters, a short explanation of the different
types can be found below:

<dl>
  <dt id="t1pks">t1pks</dt>
  <dd>Type I PKS cluster</dd>
  <dt id="transatpks">transatpks</dt>
  <dd>Trans-AT PKS cluster</dd>
  <dt id="t2pks">t2pks</dt>
  <dd>Type II PKS cluster</dd>
  <dt id="t3pks">t3pks</dt>
  <dd>Type III PKS cluster</dd>
  <dt id="otherks">otherks</dt>
  <dd>Other types of PKS cluster</dd>
  <dt id="ppysks">ppysks</dt>
  <dd>PPY-like pyrone cluster</dd>
  <dt id="arylpolyene">arylpolyene</dt>
  <dd>Aryl polyene cluster</dd>
  <dt id="resorcinol">resorcinol</dt>
  <dd>Resorcinol cluster</dd>
  <dt id="ladderane">ladderane</dt>
  <dd>Ladderane cluster</dd>
  <dt id="PUFA">PUFA</dt>
  <dd>Polyunsaturated fatty acid cluster</dd>
  <dt id="nrps">nrps</dt>
  <dd>Nonribosomal peptide synthetase cluster</dd>
  <dt id="terpene">terpene</dt>
  <dd>Terpene cluster</dd>
  <dt id="lantipeptide">lantipeptide</dt>
  <dd>Lanthipeptide cluster</dd>
  <dt id="bacteriocin">bacteriocin</dt>
  <dd>Bacteriocin or other unspecified ribosomally synthesised and post-translationally modified peptide product (RiPP) cluster</dd>
  <dt id="thiopeptide">thiopeptide</dt>
  <dd>Thiopeptide cluster</dd>
  <dt id="linaridin">linaridin</dt>
  <dd>Linaridin cluster</dd>
  <dt id="cyanobactin">cyanobactin</dt>
  <dd>Cyanobactin cluster</dd>
  <dt id="glycocin">glycocin</dt>
  <dd>Glycocin cluster</dd>
  <dt id="LAP">LAP</dt>
  <dd>Linear azol(in)e-containing peptides</dd>
  <dt id="lassopeptide">lassopeptide</dt>
  <dd>Lasso peptide cluster</dd>
  <dt id="sactipeptide">sactipeptide</dt>
  <dd>Sactipeptide cluster</dd>
  <dt id="bottromycin">bottromycin</dt>
  <dd>Bottromycin cluster</dd>
  <dt id="head_to_tail">head_to_tail</dt>
  <dd>Head-to-tail cyclised (subtilosin-like) cluster</dd>
  <dt id="microcin">microcin</dt>
  <dd>Microcin cluster</dd>
  <dt id="microviridin">microviridin</dt>
  <dd>Microviridin cluster</dd>
  <dt id="proteusin">proteusin</dt>
  <dd>Proteusin cluster</dd>
  <dt id="blactam">blactam</dt>
  <dd>&beta;-lactam cluster</dd>
  <dt id="amglyccycl">amglyccycl</dt>
  <dd>Aminoglycoside/aminocyclitol cluster</dd>
  <dt id="aminocoumarin">aminocoumarin</dt>
  <dd>Aminocoumarin cluster</dd>
  <dt id="siderophore">siderophore</dt>
  <dd>Siderophore cluster</dd>
  <dt id="ectoine">ectoine</dt>
  <dd>Ectoine cluster</dd>
  <dt id="butyrolactone">butyrolactone</dt>
  <dd>Butyrolactone cluster</dd>
  <dt id="indole">indole</dt>
  <dd>Indole cluster</dd>
  <dt id="nucleoside">nucleoside</dt>
  <dd>Nucleoside cluster</dd>
  <dt id="phosphoglycolipid">phosphoglycolipid</dt>
  <dd>Phosphoglycolipid cluster</dd>
  <dt id="melanin">melanin</dt>
  <dd>Melanin cluster</dd>
  <dt id="oligosaccharide">oligosaccharide</dt>
  <dd>Oligosaccharide cluster</dd>
  <dt id="furan">furan</dt>
  <dd>Furan cluster</dd>
  <dt id="hserlactone">hserlactone</dt>
  <dd>Homoserine lactone cluster</dd>
  <dt id="phenazine">phenazine</dt>
  <dd>Phenazine cluster</dd>
  <dt id="phosphonate">phosphonate</dt>
  <dd>Phosphonate cluster</dd>
  <dt id="fused">fused</dt>
  <dd>Pheganomycin-style protein ligase-containing cluster</dd>
  <dt id="pdbe">pbde</dt>
  <dd>Polybrominated diphenyl ether cluster</dd>
  <dt id="acyl_amino_acids">acyl_amino_acids</dt>
  <dd>N-acyl amino acid cluster</dd>
  <dt id="other">other</dt>
  <dd>Cluster containing a secondary metabolite-related protein that does not fit into any other category</dd>
  <dt id="cf_saccharide">cf_saccharide</dt>
  <dd>Putative saccharide cluster identified with the ClusterFinder algorithm</dd>
  <dt id="cf_fatty_acid">cf_fatty_acid</dt>
  <dd>Putative fatty acid cluster identified with the ClusterFinder algorithm</dd>
  <dt id="cf_putative">cf_putative</dt>
  <dd>Putative cluster of unknown type identified with the ClusterFinder algorithm</dd>
</dl>

