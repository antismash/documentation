# antiSMASH 5 feature list

##Rule-based detection of BGCs

| BGC type                     |
| ---------------------------- |
| Aminocoumarins  |
| Aminoglycosides / aminocyclitols |
| Aryl polyenes |
| atypical NRPS |
| atypical PKS |
| Bacteriocins |
| Beta-lactams |
| Beta-lactones |
| Bottromycin |
| Butyrolactones |
| ClusterFinder fatty acid |
| ClusterFinder Saccharide |
| Cyanobactins |
| (Dialkyl)resorcinols |
| Ectoines |
| Furan |
| Fused (Pheganomycin-like) |
| Glycocin |
| Head-to-tail cyclised peptide |
| Homoserine lactone |
| Indoles |
| Ladderane lipids |
| Lanthipeptides |
| Lasso peptide |
| Linaridin |
| Linear azol(in)e-containing peptides (LAPs) |
| Melanins |
| Microviridin |
| N-acyl amino acids |
| Non-ribosomal peptides |
| Nucleosides |
| Oligosaccharide |
| Other (unusual) PKS |
| Others |
| Phenazine |
| Phosphoglycolipids |
| Phosphonate |
| Polybrominated diphenyl ethers |
| Polyunsaturated fatty acids |
| Proteusin |
| Pseudopyronines |
| Sactipeptide |
| Siderophores |
| Terpene |
| Thiopeptide |
| Trans-AT type I PKS |
| Type I PKS |
| Type II PKS |
| Type III PKS |

## Rule-independent detection of BGCs
Note: This function is only available in the download/locally run version

| Tool |
| ------------- |
| ClusterFinder |

## Region specific analyses

| Analysis |
| -------- |
| Domain structure of PKSs and NRPSs |
| NRPS: A-domain specificity prediction (NRPSpredictor) |
| PKS: AT specificity prediction |
| Identification of conserved active site motifs; stereochemistry-determining motifs |
| Prediction of core chemical structure (NRPS, PKS, lanthipeptides, lasso peptides, thiopeptides) |
| smCOG secondary metabolism-related gene family prediction |
| TTA codon annotation for actinomycetes |
| Improved prediction of gene cluster borders for fungal BGCs (CASSIS) |
| Protein family detection (PFAM) search on regions |
| GO-classifications |
| Resistance gene annotation |


## Genome-wide analyses

| Analysis |
| -------- |

| Protein family detection (PFAM) search on whole genome |
| Genome comparisons |
| ClusterBlast (identification of similar clusters in sequence genomes); link to antiSMASH database |
| SubClusterBlast (identification of conserved operons with known function) |
| KnownClusterBlast (identification of similar characterized gene clusters) |


## Links to other Web-resources

| External resource |
| ----------------- |
| antiSMASH database  |
| Gene ontology |
| MIBiG repository |
| NCBI BLAST+ |
| NaPDoS |
| Norine |


## Output file formats

| Format |
| ------ |
| Genbank |
| HTML |
| JSON results file |
| Input file formats |
| FASTA (nucleotide) |
| FASTA + GFF3 |
| Genbank |
| EMBL |
| Direct download via NCBI accession number |






