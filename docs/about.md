# About antiSMASH

antiSMASH allows the rapid genome-wide identification, annotation and analysis
of secondary metabolite biosynthesis gene clusters in bacterial and fungal
genomes. It integrates and cross-links with a large number of in silico
secondary metabolite analysis tools that have been published earlier.

antiSMASH is powered by several open source tools: NCBI BLAST+, HMMer 3, Muscle
3, Glimmer 3, FastTree, TreeGraph 2, Indigo-depict, PySVG and JQuery SVG.

# antiSMASH contributors
antiSMASH is an international collaboration project, which was initially started by the labs of Wolfgang Wohlleben (University Tübignen) and Eriko Takano and Rainer Breitling (University Groningen/Manchester). Currently, the project is coordinated by Marnix Medema (University of Wageningen) and Tilmann Weber/Kai Blin (Technical University of Denmark) with numerous collaborators all over the world. 

List of contributors

# How to cite

If you have found antiSMASH useful, please cite:

* Blin, K., Wolf, T., Chevrette, M.G., Lu, X., Schwalen, C.J., Kautasar, S.A., Suarez Duran, H.G., de los Santos, E.L.C., Kim, H.U., Nave, M., et al. (2017). antiSMASH 4.0—improvements in chemistry prediction and gene cluster boundary identification. Nucleic Acids Res 45, W36-W41.
* Weber, T., Blin, K., Duddela, S., Krug, D., Kim, H.U., Bruccoleri, R., Lee, S.Y., Fischbach, M.A., Müller, R., Wohlleben, W., et al. (2015). antiSMASH 3.0-a comprehensive resource for the genome mining of biosynthetic gene clusters. Nucleic Acids Res 43, W237-W243.
* Blin, K., Medema, M.H., Kazempour, D., Fischbach, M., Breitling, R., Takano, E., and Weber, T. (2013). antiSMASH 2.0 – a versatile platform for genome mining of secondary metabolite producers. Nucleic Acids Res 41, W204-W212.
M* edema, M.H., Blin, K., Cimermancic, P., de Jager, V., Zakrzewski, P., Fischbach, M.A., Weber, T., Takano, E., and Breitling, R. (2011). antiSMASH: rapid identification, annotation and analysis of secondary metabolite biosynthesis gene clusters in bacterial and fungal genome sequences. Nucleic Acids Res 39, W339-W346.

# Further reading
* Villebro, R., Shaw, S., Blin, K., and Weber, T. (2019). Sequence-based classification of type II polyketide synthase biosynthetic gene clusters for antiSMASH. J Ind Microbiol Biotechnol, doi: 10.1007/s10295-10018-02131-10299.
* Blin, K., Kim, H.U., Medema, M.H., and Weber, T. (2017). Recent development of antiSMASH and other computational approaches to mine secondary metabolite biosynthetic gene clusters. Brief Bioinform, doi: 10.1093/bib/bbx1146.
* Blin, K., Kazempour, D., Wohlleben, W., and Weber, T. (2014). Improved lanthipeptide detection and prediction for antiSMASH. PLoS ONE 9, e89420.
